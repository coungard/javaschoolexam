package com.tsystems.javaschool.tasks.calculator;

import java.util.Arrays;
import java.util.HashMap;

class Calculator {
    private static final int MIN_PRIORITET = 1;
    private static final int NORM_PRIORITET = 2;
    private static final int MAX_PRIORITET = 3;

    private static HashMap<Integer, String> numbers = new HashMap<>();
    private static HashMap<Character, String> operators = new HashMap<>();

    private static String currentStatement;

    // true - если в нашем выражении одни плюсы да минусы
    private static boolean isSimple;

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    String evaluate(String statement) {
        if (!isValid(statement)) {
            throw new RuntimeException("THIS EXPRESSION IS NOT CORRECT! GO ZANOVO");
        }
        currentStatement = statement;
        startCalculation();
        return currentStatement;
    }

    private boolean isValid(String statement) {
        return !statement.contains(" ");
    }

    /**
     * Возвращает один простой оператор из строки, либо 0
     *
     * @param statement искомый текст выражения
     * @return оператор
     */
    private static char getOperator(String statement) {
        char[] array = statement.toCharArray();
        for (int i = 0; i < array.length; i++) {
            if (isOperator(array[i]) && i != 0)
                return array[i];
        }
        return 0;
    }

    // оператор ли?
    private static boolean isOperator(char c) {
        return c == '+' || c == '-' || c == '*' || c == '/';
    }

    /**
     * Основной цикл калькулятора. Весь процесс заключается в том, что метод обрабатывает строку (currentStatement),
     * находит в ней приоритетное выражение, вычисляет его, подставляет результат в исходную строку и проделывает эту
     * операцию до того момента, пока не находит число.
     */
    private void startCalculation() {
        while (true) {
            String fullStatement = currentStatement;
            numbers.clear(); // сбрасываем наши числа
            countNumbers(currentStatement); // подсчитывае наши числа и заносим их в map
            if (numbers.size() == 1) {
                break;
            }
            int operatorCount = 0;
            char[] array = fullStatement.toCharArray();

            for (int i = 0; i < array.length; i++) {
                if (isOperator(array[i]) && i != 0) { // проверяем на оператор
                    operatorCount++;

                    if (isSimple || getPriority(array[i]) == NORM_PRIORITET) {
                        int length1 = numbers.get(operatorCount).length(); // длина первого операнда (слева)
                        int length2 = numbers.get(operatorCount + 1).length(); // длина второго операнда (справа)

                        // находим приоритетное выражение
                        String priority = String.valueOf(Arrays.copyOfRange(array, i - length1, i + length2 + 1));
                        // вычисляем результат этот выражения
                        String solution = calculate(priority);

                        char[] before = Arrays.copyOfRange(array, 0, i - length1); // часть, что слева от приоритетного
                        char[] after = Arrays.copyOfRange(array, i + length2 + 1, array.length); // часть, что справа от приортитетного

                        //конкатенация того, что было слева, того что мы вычислили и того, что было справа.
                        currentStatement = String.valueOf(before) + solution + String.valueOf(after);
                        break;
                    }
                }
            }
        }
    }

    /**
     * Метод принимает на вход простое выражение из 1 оператора и 2 операндов и возвращает результат в виде String.
     *
     * @param expression простое выражение (7+3, 4/2, 8*10)
     * @return результат вычисления этого выражения (40/5 >> 8)
     */
    private String calculate(String expression) {
        int[] operands = getOperands(expression);
        char operator = getOperator(expression);

        int operand1 = operands[0];
        int operand2 = operands[1];

        switch (operator) {
            case '+':
                return operand1 + operand2 + "";
            case '-':
                return operand1 - operand2 + "";
            case '*':
                return operand1 * operand2 + "";
            case '/':
                return operand1 / operand2 + "";
            default:
                return null;
        }
    }

    /**
     * Возвращает приоритет оператора
     *
     * @param operator оператор
     * @return приоритет
     */
    private static int getPriority(char operator) {
        switch (operator) {
            case '(':
            case ')':
                return MAX_PRIORITET;
            case '*':
            case '/':
                return NORM_PRIORITET;

            case '+':
            case '-':
                return MIN_PRIORITET;
            default:
                return 0;
        }
    }

    /**
     * Данный метод возвращает 2 операнда из переданоого выражения
     *
     * @param text передаваемое выражение
     * @return массив int из 2-х чисел - левого и правого операнда
     */
    private static int[] getOperands(String text) {
        int[] result = new int[2];
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < text.length(); i++) {
            String element = text.substring(i, i + 1);
            if (isDigit(element) || i == 0) {
                builder.append(element);
                if (i == text.length() - 1) {
                    result[1] = Integer.parseInt(builder.toString());
                }
            } else {
                result[0] = Integer.parseInt(builder.toString());
                builder.setLength(0);
            }
        }
        return result;
    }

    /**
     * Данный метод принимает на вход строковое выражение, находит в нем все целые числа и записывает их в map <INTEGER, STRING>,
     * где INTEGER - это порядок числа в выражении, а STRING - само число в строковом представлении.
     *
     * @param text выражение
     */
    private static void countNumbers(String text) {
        StringBuilder builder = new StringBuilder();
        int numberCount = 1;
        boolean simple = true;
        char[] array = text.toCharArray();

        for (int i = 0; i < text.length(); i++) {
            String element = text.substring(i, i + 1);
            if (isDigit(element) || i == 0) {
                builder.append(element);
                if (i == text.length() - 1) {
                    numbers.put(numberCount, builder.toString());
                }
            } else {
                if (getPriority(array[i]) == NORM_PRIORITET) simple = false;
                numbers.put(numberCount, builder.toString());
                numberCount++;
                builder.setLength(0);
            }
        }
        isSimple = simple;
    }

    private static boolean isDigit(String s) throws NumberFormatException {
        try {
            Integer.parseInt(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
