package com.tsystems.javaschool.tasks.calculator;


/**
 * Created by artur, Date: 02.02.19, Time: 17:10
 */
public class CalculatorImpl extends Calculator {

    public static void main(String[] args) {
        Calculator calc = new CalculatorImpl();
        System.out.println(calc.evaluate("10/2-7+3*4"));
    }
}
